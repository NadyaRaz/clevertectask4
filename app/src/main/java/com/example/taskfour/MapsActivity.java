package com.example.taskfour;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.example.taskfour.dto.BankPointDto;
import com.example.taskfour.retrofit.BankPointsApiService;
import com.example.taskfour.retrofit.RetrofitCreator;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private final int ZOOM_LEVEL_CITY = 10;
    private final int ZOOM_LEVEL_STREET = 15;
    private final LatLng DEFAULT_COORDINATES = new LatLng(52.4345, 30.9754);
    private final int LOCATION_PERMISSION_CODE = 1;
    private GoogleMap mMap;

    private BankPointsApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        apiService = RetrofitCreator.createApiService();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(DEFAULT_COORDINATES, ZOOM_LEVEL_CITY));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_CODE);
        } else {
            populateMap();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    populateMap();
                }
                break;
            }
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @SuppressLint("MissingPermission") //запрашиваем выше
    private void populateMap() {
        mMap.setMyLocationEnabled(true);
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        try {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), ZOOM_LEVEL_STREET));
        } catch (NullPointerException exception) {
            Log.d("CurrentLocationExc", exception.getStackTrace().toString());
        }
        loadAtmList();
    }

    private void loadAtmList() {
        apiService.getBankPoints("Гомель").enqueue(new Callback<List<BankPointDto>>() {
            @Override
            public void onResponse(Call<List<BankPointDto>> call, Response<List<BankPointDto>> response) {
                if (response.body() != null) {
                    for (BankPointDto atm : response.body()) {
                        mMap.addMarker(
                                new MarkerOptions()
                                        .position(new LatLng(Double.valueOf(atm.getLatitude()), Double.valueOf(atm.getLongitude())))
                                        .title(atm.getWorkTime())
                        );
                    }
                }
            }

            @Override
            public void onFailure(Call<List<BankPointDto>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Произошла ошибка во время загрузки данных", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
