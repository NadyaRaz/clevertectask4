package com.example.taskfour.retrofit;

import com.example.taskfour.dto.BankPointDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BankPointsApiService {

    @GET("atm")
    Call<List<BankPointDto>> getBankPoints(@Query("city") String city);
}
